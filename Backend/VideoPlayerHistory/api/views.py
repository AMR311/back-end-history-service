from rest_framework import viewsets
from django.views.decorators.csrf import csrf_exempt
from .models import History
import json
from django.http import HttpResponse
from django.http import JsonResponse


@csrf_exempt
def addToHistory(request):
	decodedJson=json.loads(request.body)
	video_url=decodedJson["url"]
	newVideo= History()
	newVideo.url=video_url
	newVideo.save()
	
	response= JsonResponse(list(History.objects.all().values()), safe=False)
	response["Access-Control-Allow-Origin"] = "*"
	return response
	
@csrf_exempt
def viewAllHistory(request):
	response= JsonResponse(list(History.objects.all().values()), safe=False)
	response["Access-Control-Allow-Origin"] = "*"
	return response