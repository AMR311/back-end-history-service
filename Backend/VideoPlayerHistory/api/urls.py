from django.urls import path
from . import views

urlpatterns = [
    path('add-to-history/',views.addToHistory),
	path('view-all-history/',views.viewAllHistory),
]